package com.company;

import java.util.Scanner;



public class Main {

    public static int tryint(String arg) {
        int result = 0;
        try {
            result = Integer.parseInt(arg);
        }
        catch (Exception InputMismatchException) {
            System.out.println("Введено не целое число");
            System.exit(1);
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите количество чисел: ");
        String load = scan.next();
        int count = tryint(load);
        int sum = 0;
        int prod = 1;
        int element;
        for(int i = 0; i<count; i++){
            System.out.println("Введите целое число:");
            load = scan.next();
            element = tryint(load);
            sum += element;
            prod *= element;
        }
        System.out.printf("Сумма введённых чисел: %s, произведение: %s",sum,prod);
    }
    }
package com.company;

import java.util.Scanner;

public class Main2 {
    public static int tryint(String arg) {
        int result = 0;
        try {
            result = Integer.parseInt(arg);
        } catch (Exception InputMismatchException) {
            System.out.println("Введено не целое число");
            System.exit(1);
        }
        return result;
    }

    public static String trydiv(int arg, int divider) {
        if (arg % divider == 0) {
            return (arg + " делится на " + divider);
        } else {
            return (arg + " не делится на " + divider);
        }
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите количество чисел: ");
        String load = scan.next();
        int count = tryint(load);
        int element;
        for (int i = 0; i < count; i++) {
            System.out.println("Введите целое число:");
            load = scan.next();
            element = tryint(load);
            System.out.println(trydiv(element, 3));
            System.out.println(trydiv(element,9));
        }
    }
}
